package com.chama.nearme.model

import com.google.gson.annotations.SerializedName

enum class PlaceType(val value: String) {
    CAFE("cafe"),
    RESTAURANT("restaurant"),
    BAR("bar")
}

data class PlacesResponse(
    @SerializedName("results")
    val places: Array<Place>
)

data class Place(
    @SerializedName("name")
    val name: String,
    @SerializedName("opening_hours")
    val openingHours: OpeningHours,
    @SerializedName("rating")
    val rating: Float,
    @SerializedName("geometry")
    val geometry: Geometry
)

data class OpeningHours(
    @SerializedName("open_now")
    val openNow: Boolean
)

data class Geometry(
    @SerializedName("location")
    val location: PlaceLocation
)

data class PlaceLocation(
    @SerializedName("lat")
    val latitude: Double,
    @SerializedName("lng")
    val longitude: Double
) {
    override fun toString(): String {
        return "${latitude},${longitude}"
    }
}

