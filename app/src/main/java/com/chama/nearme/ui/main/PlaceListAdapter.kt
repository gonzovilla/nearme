package com.chama.nearme.ui.main

import android.content.Context
import android.location.Location
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.chama.nearme.R
import com.chama.nearme.model.Place

class PlaceListAdapter(
    private val myDataset: Array<Place>,
    val userLocation: Location,
    val itemClickListener: OnItemClickListener,
    val context: Context
) :
    RecyclerView.Adapter<PlaceListAdapter.MyViewHolder>() {

    class MyViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById(R.id.name_textview) as TextView
        val rating = itemView.findViewById(R.id.rating_textview) as TextView
        val openNow = itemView.findViewById(R.id.open_textview) as TextView
        val distance = itemView.findViewById(R.id.distance_textview) as TextView

        fun bind(
            place: Place,
            userLocation: Location,
            itemClickListener: OnItemClickListener,
            context: Context
        ) {
            name.text = place.name
            rating.text =
                String.format(context.getString(R.string.place_rating_format), place.rating)

            try {
                if (place.openingHours.openNow) {
                    openNow.text = context.getString(R.string.place_open)
                    openNow.setTextColor(ContextCompat.getColor(context, R.color.colorOpen));
                } else {
                    openNow.text = context.getString(R.string.place_closed)
                    openNow.setTextColor(ContextCompat.getColor(context, R.color.colorClosed));
                }
            } catch (throwable: Throwable) {
                openNow.text = ""
            }

            val placeLocation = Location("placeLocation")
            placeLocation.latitude = place.geometry.location.latitude
            placeLocation.longitude = place.geometry.location.longitude
            val geoDistance = userLocation.distanceTo(placeLocation)
            distance.text =
                String.format(context.getString(R.string.place_distance_format), geoDistance)

            itemView.setOnClickListener {
                itemClickListener.onItemClicked(place)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MyViewHolder(layoutInflater.inflate(R.layout.place_list_item, parent, false))
    }

    override fun onBindViewHolder(
        holder: MyViewHolder,
        position: Int
    ) {
        val item = myDataset.get(position)
        holder.bind(item, userLocation, itemClickListener, context)
    }

    override fun getItemCount() = myDataset.size

    interface OnItemClickListener {
        fun onItemClicked(place: Place)
    }
}
