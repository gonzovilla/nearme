package com.chama.nearme.ui.main

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chama.nearme.MainActivity
import com.chama.nearme.R
import com.chama.nearme.model.Place
import com.chama.nearme.model.PlaceType
import com.chama.nearme.utils.RxBus.subscribeToBars
import com.chama.nearme.utils.RxBus.subscribeToCafes
import com.chama.nearme.utils.RxBus.subscribeToRestaurants
import com.chama.nearme.utils.RxBus.subscribeToUserLocation
import io.reactivex.disposables.CompositeDisposable

class PlacesFragment : Fragment(), PlaceListAdapter.OnItemClickListener {

    private lateinit var type: String

    private val compositeDisposable = CompositeDisposable()

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var userLocation: Location

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments?.getString(ARG_TYPE_PLACE) ?: PlaceType.values().get(0).value
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        recyclerView = root.findViewById(R.id.recycler_view)
        compositeDisposable.add(subscribeToUserLocation(this::setUserLocation))
        when (type) {
            PlaceType.BAR.value -> compositeDisposable.add(subscribeToBars(this::setUpList))
            PlaceType.RESTAURANT.value -> compositeDisposable.add(subscribeToRestaurants(this::setUpList))
            PlaceType.CAFE.value -> compositeDisposable.add(subscribeToCafes(this::setUpList))
        }
        return root
    }

    private fun setUpList(places: Array<Place>) {

        viewManager = LinearLayoutManager(context)
        viewAdapter = PlaceListAdapter(places, userLocation, this, requireContext())

        recyclerView.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }

    }

    private fun setUserLocation(location: Location) {
        userLocation = location
    }

    override fun onItemClicked(place: Place) {

        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now

        (activity as MainActivity).goToDetailView(place)

    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.dispose()
    }

    companion object {

        private const val ARG_TYPE_PLACE = "type_place"

        @JvmStatic
        fun newInstance(sectionNumber: Int, type: PlaceType): PlacesFragment {
            return PlacesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TYPE_PLACE, type.value)
                }
            }
        }
    }

}