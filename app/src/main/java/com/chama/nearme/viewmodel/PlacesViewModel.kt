package com.chama.nearme.viewmodel

import android.content.Context
import android.location.Location
import com.chama.nearme.model.Place
import com.chama.nearme.model.PlaceLocation
import com.chama.nearme.model.PlaceType
import com.chama.nearme.network.PlacesRepository
import com.google.android.gms.location.LocationServices
import io.reactivex.Single

class PlacesViewModel(val placesRepository: PlacesRepository) {

    fun getUserLocation(context: Context): Single<Location> {
        val fusedClient = LocationServices.getFusedLocationProviderClient(context)
        return Single.create<Location> { emitter ->
            fusedClient.lastLocation.addOnSuccessListener { location ->
                emitter.onSuccess(location)
            }.addOnFailureListener { error ->
                emitter.onError(error)
            }
        }
    }

    fun getNearbyPlaces(location: Location, type: PlaceType): Single<Array<Place>> {

        return placesRepository.getNearbyPlaces(
            PlaceLocation(
                location.latitude,
                location.longitude
            ), type
        )
            .flatMap { response -> Single.just(response.places) }
    }

}