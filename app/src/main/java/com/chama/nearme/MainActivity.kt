package com.chama.nearme

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.chama.nearme.model.Place
import com.chama.nearme.model.PlaceType
import com.chama.nearme.network.NetworkManager
import com.chama.nearme.network.PlaceApi
import com.chama.nearme.network.PlacesRepository
import com.chama.nearme.ui.main.SectionsPagerAdapter
import com.chama.nearme.utils.RxBus.publishBars
import com.chama.nearme.utils.RxBus.publishCafes
import com.chama.nearme.utils.RxBus.publishRestaurants
import com.chama.nearme.utils.RxBus.publishUserLocation
import com.chama.nearme.viewmodel.PlacesViewModel
import com.google.android.material.tabs.TabLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"

        enum class DialogType {
            ERROR,
            NO_PERMISSIONS
        }
    }

    private val compositeDisposable = CompositeDisposable()

    private lateinit var placeApi: PlaceApi
    private lateinit var placesRepository: PlacesRepository
    private lateinit var placesViewModel: PlacesViewModel

    private lateinit var progressBar: ProgressBar
    private var dialogShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        progressBar = findViewById(R.id.progress_bar)

        placeApi = NetworkManager().getRetrofit().create(PlaceApi::class.java)
        placesRepository = PlacesRepository(placeApi)
        placesViewModel = PlacesViewModel(placesRepository)

        checkPermissionAndFetchPlaces()
    }

    private fun checkPermissionAndFetchPlaces() {

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                123
            )
        } else {
            getPlacesNearMe()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissions.size > 0 && permissions[0] == "android.permission.ACCESS_FINE_LOCATION" && grantResults[0] >= 0
        ) {
            getPlacesNearMe()
        } else {
            showAlertDialog(DialogType.NO_PERMISSIONS)
        }
    }

    private fun getPlacesNearMe() {
        progressBar.visibility = View.VISIBLE

        PlaceType.values().forEach {
            compositeDisposable.add(
                placesViewModel.getUserLocation(this).flatMap { location ->
                    val userLocation = Location("userLocation")
                    userLocation.latitude = location.latitude
                    userLocation.longitude = location.longitude
                    publishUserLocation(userLocation)
                    placesViewModel.getNearbyPlaces(
                        userLocation,
                        it
                    )
                }.observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ places ->
                        progressBar.visibility = View.GONE
                        when (it) {
                            PlaceType.BAR -> publishBars(places)
                            PlaceType.RESTAURANT -> publishRestaurants(places)
                            PlaceType.CAFE -> publishCafes(places)
                        }
                    }, { throwable ->
                        handleError(throwable, it)
                    })
            )
        }

    }

    private fun handleError(throwable: Throwable, type: PlaceType) {
        progressBar.visibility = View.GONE
        Log.e(TAG, "Error retrieving ${type.value}: ${throwable.localizedMessage}")
        if (!dialogShown) {
            dialogShown = true
            showAlertDialog(DialogType.ERROR)
        }
    }

    private fun showAlertDialog(dialogType: DialogType) {
        val builder = AlertDialog.Builder(this)

        with(builder)
        {
            when (dialogType) {
                DialogType.ERROR -> {
                    setTitle(getString(R.string.error_dialog_title))
                    setMessage(getString(R.string.error_dialog_message))
                }
                DialogType.NO_PERMISSIONS -> {
                    setTitle(getString(R.string.permissions_dialog_title))
                    setMessage(getString(R.string.permissions_dialog_message))
                }
            }
            setPositiveButton(
                android.R.string.ok
            ) { dialogInterface, i ->
                dialogShown = false
            }
            show()
        }
    }

    fun goToDetailView(place: Place) {

        val intent = Intent(this, MapsActivity::class.java)
        intent.putExtra("EXTRA_NAME", place.name)
        intent.putExtra("EXTRA_LAT", place.geometry.location.latitude)
        intent.putExtra("EXTRA_LON", place.geometry.location.longitude)
        startActivity(intent)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_refresh) {
            checkPermissionAndFetchPlaces()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}