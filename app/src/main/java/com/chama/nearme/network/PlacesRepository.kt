package com.chama.nearme.network

import com.chama.nearme.model.PlaceLocation
import com.chama.nearme.model.PlaceType
import com.chama.nearme.model.PlacesResponse
import io.reactivex.Single

class PlacesRepository(val placeApi: PlaceApi) {

    fun getNearbyPlaces(location: PlaceLocation, type: PlaceType): Single<PlacesResponse> {
        return placeApi.getNearbyPlaces(location.toString(), type.value)
    }
}