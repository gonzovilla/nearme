package com.chama.nearme.network

import com.chama.nearme.model.PlacesResponse
import com.chama.nearme.utils.apiKey
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PlaceApi {

    @GET("nearbysearch/json?rankby=distance&key=${apiKey}")
    fun getNearbyPlaces(@Query("location") location: String, @Query("type") type: String): Single<PlacesResponse>

}