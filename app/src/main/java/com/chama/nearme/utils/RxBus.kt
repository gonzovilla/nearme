package com.chama.nearme.utils

import android.location.Location
import com.chama.nearme.model.Place
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import kotlin.reflect.KFunction1

object RxBus {

    private val cafesAround : BehaviorSubject<Array<Place>> = BehaviorSubject.create()
    fun publishCafes(cafes: Array<Place>) = cafesAround.onNext(cafes)
    fun subscribeToCafes(action: KFunction1<@ParameterName(name = "places") Array<Place>, Unit>): Disposable = cafesAround.subscribe(action)

    private val restaurantsAround : BehaviorSubject<Array<Place>> = BehaviorSubject.create()
    fun publishRestaurants(restaurants: Array<Place>) = restaurantsAround.onNext(restaurants)
    fun subscribeToRestaurants(action: KFunction1<@ParameterName(name = "places") Array<Place>, Unit>): Disposable = restaurantsAround.subscribe(action)

    private val barsAround : BehaviorSubject<Array<Place>> = BehaviorSubject.create()
    fun publishBars(bars: Array<Place>) = barsAround.onNext(bars)
    fun subscribeToBars(action: KFunction1<@ParameterName(name = "places") Array<Place>, Unit>): Disposable = barsAround.subscribe(action)

    private val userLocation : BehaviorSubject<Location> = BehaviorSubject.create()
    fun publishUserLocation(location: Location) = userLocation.onNext(location)
    fun subscribeToUserLocation(action: KFunction1<@ParameterName(name = "userLocation") Location, Unit>): Disposable = userLocation.subscribe(action)

}