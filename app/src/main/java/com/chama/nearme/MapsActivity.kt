package com.chama.nearme

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private val MAP_ZOOM_LEVEL = 14

    private lateinit var mMap: GoogleMap

    lateinit var placeName: String
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    lateinit var place: LatLng

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        placeName = intent.getStringExtra("EXTRA_NAME")
        latitude = intent.getDoubleExtra("EXTRA_LAT", 0.0)
        longitude = intent.getDoubleExtra("EXTRA_LON", 0.0)
        place = LatLng(latitude, longitude)

        title = placeName

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.isMyLocationEnabled = true

        mMap.addMarker(MarkerOptions().position(place).title(placeName))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place, MAP_ZOOM_LEVEL.toFloat()))
    }
}
