package com.chama.nearme

import android.location.Location
import com.chama.nearme.model.Place
import com.chama.nearme.model.PlaceLocation
import com.chama.nearme.model.PlaceType
import com.chama.nearme.model.PlacesResponse
import com.chama.nearme.network.PlaceApi
import com.chama.nearme.network.PlacesRepository
import com.chama.nearme.viewmodel.PlacesViewModel
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

class PlacesViewModelTest {

    lateinit var placesViewModel: PlacesViewModel
    lateinit var placesRepository: PlacesRepository
    lateinit var placeApi: PlaceApi

    lateinit var place: Place

    lateinit var placeLocation: Location
    lateinit var placeType: PlaceType


    @Before
    fun setup() {

        placeApi = mock()
        placesRepository = mock()
        placesViewModel = PlacesViewModel(placesRepository)

        place = mock()
        placeLocation = mock()
        placeType = mock()
    }

    @Test
    fun `no data on api returns empty array`() {
        `when`(
            placesRepository.getNearbyPlaces(
                PlaceLocation(
                    placeLocation.latitude,
                    placeLocation.longitude
                ), placeType
            )
        ).thenReturn(
            Single.just(
                PlacesResponse(
                    emptyArray()
                )
            )
        )

        placesViewModel.getNearbyPlaces(placeLocation, placeType).test()
            .assertValue { it.isEmpty() }
    }

    @Test
    fun `one item on api returns one item`() {
        `when`(
            placesRepository.getNearbyPlaces(
                PlaceLocation(
                    placeLocation.latitude,
                    placeLocation.longitude
                ), placeType
            )
        ).thenReturn(Single.just(PlacesResponse(arrayOf(place))))

        placesViewModel.getNearbyPlaces(placeLocation, placeType).test()
            .assertValueCount(1)
            .assertValue { it.size == 1 }
    }
}
