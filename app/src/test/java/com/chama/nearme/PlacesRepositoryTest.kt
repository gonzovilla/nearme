package com.chama.nearme

import com.chama.nearme.model.Place
import com.chama.nearme.model.PlaceLocation
import com.chama.nearme.model.PlaceType
import com.chama.nearme.model.PlacesResponse
import com.chama.nearme.network.PlaceApi
import com.chama.nearme.network.PlacesRepository
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

class PlacesRepositoryTest {

    lateinit var placesRepository: PlacesRepository
    lateinit var placeApi: PlaceApi

    lateinit var place: Place

    lateinit var placeLocation: PlaceLocation
    lateinit var placeType: PlaceType


    @Before
    fun setup() {
        placeApi = mock()
        placesRepository = PlacesRepository(placeApi)

        place = mock()
        placeLocation = mock()
        placeType = mock()
    }

    @Test
    fun `no data on api returns empty array`() {
        `when`(placeApi.getNearbyPlaces(placeLocation.toString(), placeType.value)).thenReturn(
            Single.just(PlacesResponse(emptyArray()))
        )

        placesRepository.getNearbyPlaces(placeLocation, placeType).test()
            .assertValue { it.places.isEmpty() }
    }

    @Test
    fun `one item on api returns one item`() {
        `when`(placeApi.getNearbyPlaces(placeLocation.toString(), placeType.value)).thenReturn(
            Single.just(PlacesResponse(arrayOf(place)))
        )

        placesRepository.getNearbyPlaces(placeLocation, placeType).test()
            .assertValueCount(1)
            .assertValue { it.places.size == 1 }
    }
}
